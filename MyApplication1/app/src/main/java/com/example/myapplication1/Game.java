package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Game extends AppCompatActivity implements View.OnClickListener {

    private TextView player1name;
    private TextView player2name;

    private Button buttons[][]=new Button[3][3];
    private Button matchHistory;

    private TextView score1tv;
    private TextView score2tv;

    private String name1;
    private String name2;

    private boolean playerTurn = true;
    private int turnCount = 0;

    private int score1 = 0;
    private int score2 = 0;

    ArrayList<MatchData> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initializeUI();
    } ///ovo rip

    private void initializeUI() {
        this.matchHistory = (Button) findViewById(R.id.matchHistoryBtn);

        this.score1tv = (TextView) findViewById(R.id.score1tv);
        this.score2tv = (TextView) findViewById(R.id.score2tv);

        this.player1name = (TextView) findViewById(R.id.player1tv);
        this.player2name = (TextView) findViewById(R.id.player2tv);

        name1 = getIntent().getStringExtra("Player1");
        name2 = getIntent().getStringExtra("Player2");

        this.player1name.setText(name1);
        this.player2name.setText(name2);

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                String buttonID = "btn"+i+j;
                int resourceID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = (Button) findViewById(resourceID);
                buttons[i][j].setOnClickListener(this);
            }
        }

        matchHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this,MatchHistory.class);
                intent.putExtra("Match Data", dataList);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(!((Button)v).getText().toString().equals("")){
            return;
        }

        if(playerTurn){
            ((Button)v).setText("X");
        }else{
            ((Button)v).setText("O");
        }

        turnCount++;

        if(checkForWinner()) {
            if (playerTurn) {
                player1Won();
            } else {
                player2Won();
            }
        }
        else if (turnCount == 9){
            draw();
        } else {
            playerTurn = !playerTurn;
        }
    }

    private boolean checkForWinner() {
        String[][] buttonTags = new String[3][3];

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                buttonTags[i][j] = buttons[i][j].getText().toString();
            }
        }

        for(int i=0; i<3; i++){
            if(buttonTags[i][0].equals(buttonTags[i][1])
                    && buttonTags[i][0].equals(buttonTags[i][2])
                    && !buttonTags[i][0].equals("")){
                return true;
            }
        }

        for(int i=0; i<3; i++){
            if(buttonTags[0][i].equals(buttonTags[1][i])
                    && buttonTags[0][i].equals(buttonTags[2][i])
                    && !buttonTags[0][i].equals("")){
                return true;
            }
        }

        if(buttonTags[0][0].equals(buttonTags[1][1])
                && buttonTags[0][0].equals(buttonTags[2][2])
                && !buttonTags[0][0].equals("")){
            return true;
        }

        if(buttonTags[0][2].equals(buttonTags[1][1])
                && buttonTags[0][2].equals(buttonTags[2][0])
                && !buttonTags[0][2].equals("")){
            return true;
        }
        return false;
    }

    private void player1Won() {
        score1++;
        Toast.makeText(this, name1 + " won!", Toast.LENGTH_SHORT).show();
        score1tv.setText(Integer.toString(score1));
        addMatchData(name1);
        resetGame();
    }

    private void player2Won() {
        score2++;
        Toast.makeText(this, name2 + " won!", Toast.LENGTH_SHORT).show();
        score2tv.setText(Integer.toString(score2));
        addMatchData(name2);
        resetGame();
    }

    private void draw() {
        Toast.makeText(this,"Draw!",Toast.LENGTH_SHORT).show();
        addMatchData("");
        resetGame();
    }

    private void resetGame() {
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                buttons[i][j].setText("");
            }
        }

        turnCount = 0;
        playerTurn = true;
    }

    private void addMatchData(String name1){
        dataList.add(new MatchData(name1, turnCount));
    }
}