package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText player1name;
    private EditText player2name;
    private Button playButton;

    private String name1;
    private String name2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        this.player1name = (EditText) findViewById(R.id.player1_et);
        this.player2name = (EditText) findViewById(R.id.player2_et);
        this.playButton = (Button) findViewById(R.id.play_btn);

        playButton.setEnabled(false);

        player1name.addTextChangedListener(nameTextWatcher);
        player2name.addTextChangedListener(nameTextWatcher);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Game.class);
                intent.putExtra("Player1", name1);
                intent.putExtra("Player2", name2);
                startActivity(intent);
            }
        });
    }

    private TextWatcher nameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            name1 = player1name.getText().toString();
            name2 = player2name.getText().toString();

            playButton.setEnabled(!name1.isEmpty() && !name2.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) { }
    };
}