package com.example.myapplication1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MatchHistory extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<Match> matchList = new ArrayList<>();

    ArrayList<MatchData> dataList = new ArrayList<>();
    MatchData currentData = new MatchData();

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_history);
        getData();
        initializeUI();

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new MatchAdapter(matchList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initializeUI() {
        for(int i=0; i<dataList.size(); i++){
            currentData = dataList.get(i);
            if(currentData.getTurnCount() == 9){
                matchList.add(new Match(R.drawable.draw,"Draw!","Number of turns: 9"));
            }
            else{
                if(currentData.getTurnCount()%2 == 0){
                    matchList.add(new Match(R.drawable.sign_o, currentData.getName() + " won!", "Number of turns: " + Integer.toString(currentData.getTurnCount())));
                } else {
                    matchList.add(new Match(R.drawable.sign_x, currentData.getName() + " won!", "Number of turns: " + Integer.toString(currentData.getTurnCount())));
                }
            }
        }
        button = (Button) findViewById(R.id.end_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MatchHistory.this,End.class);
                startActivity(intent);
            }
        });
    }

    private void getData(){
        Intent intent = getIntent();
        dataList = (ArrayList<MatchData>) intent.getSerializableExtra("Match Data");
    }
}