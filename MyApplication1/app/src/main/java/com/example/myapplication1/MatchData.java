package com.example.myapplication1;

import java.io.Serializable;

public class MatchData implements Serializable {
    private String mName;
    private int mTurnCount;

    public MatchData(){
        mName="";
        mTurnCount=0;
    }

    public MatchData(String name, int turnCount){
        mName = name;
        mTurnCount = turnCount;
    }

    public String getName(){
        return mName;
    }

    public int getTurnCount(){
        return mTurnCount;
    }
}
