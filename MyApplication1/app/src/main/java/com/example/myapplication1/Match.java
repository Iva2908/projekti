package com.example.myapplication1;

public class Match {
    private int mImageResource;
    private String mText1;
    private String mText2;

    public Match(){
        mImageResource = 0;
        mText1 = "";
        mText2 = "";
    }

    public Match(int imageResource, String text1, String text2){
        mImageResource = imageResource;
        mText1 = text1;
        mText2 = text2;
    }

    public int getImageResource(){
        return mImageResource;
    }

    public String getText1(){
        return mText1;
    }

    public String getText2(){
        return mText2;
    }
}
